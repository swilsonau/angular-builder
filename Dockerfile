FROM node:11

RUN apt-get --assume-yes update && apt-get --assume-yes install rsync awscli jq
RUN npm install -g @angular/cli && npm install @sentry/cli

CMD [ "node" ]